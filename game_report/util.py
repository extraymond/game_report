from io import StringIO

import ujson


async def stream_json(cur):
    file = StringIO()
    docs = [i async for i in cur]
    ujson.dump(docs, file)

    yield file.getvalue().encode('utf-8')
    file.close()
