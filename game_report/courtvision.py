import numpy as np
from loguru import logger
from pymongo import GEO2D, GEOSPHERE, MongoClient

client = MongoClient()
db = client.geo_data
col = db["geo_records"]
db.drop_collection("geo_records")
hoop = {"name": "hoop", "location": [1.575, 3.05]}
db.geo_records.insert_one(hoop)

points = [
    {"name": "shot", "location": [x, y]}
    for x in np.linspace(0, 14, 100)
    for y in np.linspace(0, 15, 100)
]

db.geo_records.insert_many(points)

db.geo_records.create_index([("location", GEO2D)])


for doc in db.geo_records.aggregate([{"$count": "size"}]):
    size = doc["size"]
    break

pipeline = [
    {
        "$geoNear": {
            "near": [1.575, 7.5],
            "distanceField": "range",
            "key": "location",
            "num": size,
        }
    },
    {
        "$addFields": {
            "zone": {
                "$switch": {
                    "branches": [
                        {"case": {"$lte": ["$range", 1.25]}, "then": "inner"},
                        {"case": {"$lte": ["$range", 2.45]}, "then": "short"},
                        {"case": {"$lte": ["$range", 4.88]}, "then": "mid"},
                        {
                            "case": {
                                "$cond": {
                                    "if": {
                                        "$lte": [{"$arrayElemAt": ["$location", 0]}, 3]
                                    },
                                    "then": {
                                        "$and": [
                                            {
                                                "$gte": [
                                                    {"$arrayElemAt": ["$location", 1]},
                                                    0.9,
                                                ]
                                            },
                                            {
                                                "$lte": [
                                                    {"$arrayElemAt": ["$location", 1]},
                                                    14.1,
                                                ]
                                            },
                                        ]
                                    },
                                    "else": {"$lte": ["$range", 6.75]},
                                }
                            },
                            "then": "long",
                        },
                    ],
                    "default": "3PT",
                }
            }
        }
    },
    {"$group": {"_id": "$zone", "shots": {"$sum": 1}}},
    {
        "$lookup": {
            "from": "geo_records",
            "pipeline": [{"$count": "size"}],
            "as": "total",
        }
    },
    {"$unwind": "$total"},
    {"$addFields": {"percentage": {"$divide": ["$shots", "$total.size"]}}}
    # {
    #     "$addFields": {
    #         "info": {
    #             "$mergeObjects": [
    #                 {"zone": "$zone", "range": "$range"},
    #                 {"$arrayToObject": {"$zip": {"inputs": [["x", "y"], "$location"]}}},
    #             ]
    #         }
    #     }
    # },
    # {"$replaceRoot": {"newRoot": "$info"}},
]

cur = db.geo_records.aggregate(pipeline)

for doc in cur:
    logger.info(doc)
