import graphene
from graphql.execution.executors.asyncio import AsyncioExecutor
from starlette.applications import Starlette
from starlette.graphql import GraphQLApp
from starlette.middleware.cors import CORSMiddleware
from starlette.middleware.gzip import GZipMiddleware
from starlette.responses import UJSONResponse

from game_report.resources import filemanager, gql, info, team

app = Starlette()
app.add_middleware(CORSMiddleware, allow_origins=["*"])
app.add_middleware(GZipMiddleware, minimum_size=1000)
app.mount("/info", info.app)
app.mount("/team", team.app)
app.mount("/files", filemanager.app)


app.add_route("/gql", gql.app)


@app.exception_handler(404)
async def not_found(request, exc):
    return UJSONResponse({"result": "not found."}, status_code=exc.status_code)
