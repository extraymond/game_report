import ffmpeg
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
import os

base = '/media/backlog/Videos/成大校女籃/2018-19/大專杯/unfinished'
games = os.listdir(base)


def merge_game(folder):
    files = sorted(os.listdir(f'{base}/{folder}'))

    with open(f'{base}/{folder}/input.txt', 'w') as writer:
        rv = ""

        for file in files:
            if ('txt' not in file) & ('concat' not in file):
                rv += f"file {base}/{folder}/{file}\n"

        writer.write(rv)

    fmt = files[0].split('.')[-1]

    ffmpeg.input(f'{base}/{folder}/input.txt', format='concat',
                 safe=0).output(f'{base}/{folder}/concat.{fmt}',
                                c='copy').run()


with ThreadPoolExecutor(8) as executor:
    executor.map(merge_game, games)
