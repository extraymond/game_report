import click
import numpy as np
import pandas as pd
import pendulum
from pymongo import MongoClient
from splinter import Browser


class UBA:
    def __init__(self):
        self.root = "http://uba.tw/107"

        client = MongoClient()
        db = client["games_report"]

        self.teams = db["teams"]
        self.games = db["games"]
        self.players = db["players"]
        self.events = db["events"]

    def __enter__(self):
        self.browser = Browser(
            "chrome",
            headless=True,
            **{"executable_path": "/home/extraymond/bin/chromedriver/chromedriver"},
        ).__enter__()

        return self

    def __exit__(self, *args, **kwargs):
        self.browser.__exit__(*args, **kwargs)

    def go_to_team(self, team_id):
        self.browser.visit(f"{self.root}/公開女一級/Team/Index/{team_id}")

    def go_to_game(self, game_id):
        self.browser.visit(f"{self.root}/Schedule/Details?scheduleId={game_id}")

    def go_to_play(self, game_id):
        self.browser.visit(f"{self.root}/PlayByPlay/Index?scheduleId={game_id}")

    def fetch_teams(self):

        self.teams.drop()

        self.browser.visit(self.root)
        self.browser.find_by_text("公開女一級").click()
        self.browser.find_by_text("球隊資訊").click()

        teams = self.browser.find_by_css(
            'div[class="school_content"]'
        ).first.find_by_tag("li")
        team_profiles = [
            {
                "_id": team.find_by_tag("a")[0]["href"].split("Index/")[1],
                "team": team.find_by_tag("span")[-1].text,
            }
            for team in teams
        ]

        self.teams.insert_many(team_profiles)
        self.teams.create_index("team")

    def fetch_players(self):

        self.players.drop()
        teams = self.teams.distinct("teams")

        for team in self.teams.distinct("_id"):

            self.go_to_team(team)
            df = pd.read_html(
                self.browser.find_by_id("players")[0].html, flavor="html5lib"
            )[0]
            df.columns = df.iloc[0, :]
            df = df.iloc[1:-1]
            df = df.rename(
                columns=dict(
                    zip(
                        ["背號", "姓名", "年級", "身高", "體重"],
                        ["jersey", "player", "grade", "height", "weight"],
                    )
                )
            )
            df["team"] = team
            self.players.insert_many(df.to_dict("records"))

        for i in ["player", "height", "weight", "team"]:
            self.players.create_index(i)

    def fetch_games(self):

        self.games.drop()

        for team in self.teams.distinct("_id"):

            self.go_to_team(team)

            n_games = len(self.browser.find_by_id("全學年度戰績")[0].find_by_tag("tr")) - 4

            for i in range(2, n_games + 2):

                pre = self.browser.url

                games_block = list(
                    self.browser.find_by_id("全學年度戰績")[0].find_by_tag("tr")
                )
                link = games_block[i].find_by_tag("a")[0]

                while self.browser.url == pre:

                    if not link["href"]:
                        link.click()

                else:
                    game_id = self.browser.url.split("scheduleId=")[1]

                    if game_id not in self.games.distinct("_id"):

                        game_doc = {"_id": game_id}

                        result = game_parser.get_result(self.browser)
                        box = game_parser.get_box(self.browser)

                        game_doc["result"] = result.to_dict("records")
                        game_doc["box"] = box.to_dict("records")
                        game_doc["events"] = "Play-By-Play" in self.browser.html

                        self.games.insert_one(game_doc)

                self.browser.back()

        for index in ["box.player", "box.team", "box.先發", "box.時間"]:
            self.games.create_index(index)

    def fetch_events(self):

        self.events.drop()

        for game in self.games.distinct("_id", filter={"events": True}):

            self.go_to_play(game)
            events = game_parser.get_play(self.browser)
            events["game"] = game

            self.events.insert_many(events.to_dict("records"))

        for index in ["game", "seconds", "team", "player", "events", "events.type"]:
            self.events.create_index(index)

        #         clean up rebound formats
        cur = self.events.aggregate(
            [
                {"$match": {"$expr": {"$in": ["籃板", "$events.type"]}}},
                {"$unwind": "$events"},
                {"$sort": {"game": 1, "team": 1, "player": 1}},
                {
                    "$group": {
                        "_id": {"game": "$game", "player": "$player"},
                        "current_events": {
                            "$push": {"$mergeObjects": [{"e_id": "$_id"}, "$events"]}
                        },
                    }
                },
                {"$project": {"current_events.type": 0}},
                {
                    "$addFields": {
                        "pre_events": {
                            "$concatArrays": [
                                [{"e_id": None, "累計進攻": 0, "累計防守": 0}],
                                "$current_events",
                            ]
                        }
                    }
                },
                {
                    "$addFields": {
                        "pair": {"$zip": {"inputs": ["$current_events", "$pre_events"]}}
                    }
                },
                {
                    "$addFields": {
                        "verdict": {
                            "$map": {
                                "input": "$pair",
                                "in": {
                                    "$let": {
                                        "vars": {
                                            "now": {"$arrayElemAt": ["$$this", 0]},
                                            "pre": {"$arrayElemAt": ["$$this", 1]},
                                        },
                                        "in": {
                                            "$cond": {
                                                "if": {
                                                    "$gt": ["$$now.累計進攻", "$$pre.累計進攻"]
                                                },
                                                "then": {
                                                    "_id": "$$now.e_id",
                                                    "type": "進攻籃板",
                                                },
                                                "else": {
                                                    "_id": "$$now.e_id",
                                                    "type": "防守籃板",
                                                },
                                            }
                                        },
                                    }
                                },
                            }
                        }
                    }
                },
                {"$unwind": "$verdict"},
                {"$project": {"_id": "$verdict._id", "reb_type": "$verdict.type"}},
            ]
        )

        reb_updates = list(cur)
        for doc in reb_updates:
            self.events.update_one(
                {"_id": doc.pop("_id")}, {"$set": {"events": [{"type": "籃板", **doc}]}}
            )


class game_parser:
    @staticmethod
    def get_result(browser):

        game_box = pd.read_html(
            browser.find_by_css('div[class="boxscore_block"]')[-1].html,
            flavor="html5lib",
        )[1]

        game_box.columns = (
            ["team"]
            + game_box.iloc[0, 1:-1]
            .apply(lambda x: f"QTR_{int(x)+1}" if "OT" not in str(x) else x)
            .tolist()
            + ["total"]
        )

        game_box = game_box.iloc[1:]
        return game_box.dropna(axis=1)

    @staticmethod
    def get_box(browser):
        tboxs = browser.find_by_css('div[class="boxscore_content"]')
        dfs = []

        for tbox in tboxs:
            team = tbox.find_by_tag("div")[0].value

            df = pd.read_html(
                tbox.find_by_tag("table")[0].find_by_xpath("..")[0].html,
                flavor="html5lib",
            )[0]
            df.columns = ["jersey", "player"] + df.iloc[1, 2:].tolist()
            df = df.iloc[3:-1, :].copy()
            df.loc[:, "先發"] = df["先發"].apply(lambda x: True if x == "X" else False)
            for shot in ["二分", "三分", "罰球"]:
                df[f"{shot}命中"] = df[shot].apply(lambda x: x.split("-")[0])
                df[f"{shot}出手"] = df[shot].apply(lambda x: x.split("-")[1])
            df.columns = [
                i if i not in ["進攻", "防守", "總計"] else f"{i}籃板" for i in df.columns
            ]
            df = df.loc[
                :, [i for i in df.columns if i not in list(df.columns)[4:9]]
            ].copy()
            for cl in df.columns:
                if cl not in ["player", "先發", "時間"]:
                    df.loc[:, cl] = df[cl].astype("int")
                elif cl == "時間":
                    df.loc[:, cl] = df[cl].astype("float")
            df["team"] = team
            dfs.append(df)
        return pd.concat(dfs).reset_index(drop=True)

    @staticmethod
    def get_play(browser):
        df = pd.read_html(browser.html, flavor="html5lib")[0]
        df.columns = df.iloc[0]
        df = df.iloc[:0:-1, :]
        df["seconds"] = df.apply(
            lambda x: (
                pendulum.time(hour=0, minute=10 * int(x["節數"]))
                - pendulum.parse("00:" + x["時間"], exact=True)
            ).total_seconds(),
            axis=1,
        )
        df = df.rename(columns={"場上球員": "player", "球隊": "team"})

        df["events"] = df.apply(game_parser.parse_types, axis=1)
        df.loc[:, "player"] = df["player"].apply(lambda x: None if pd.isna(x) else x)
        return df.loc[:, [i for i in df.columns if i not in ["文字直播", "順序"]]].rename(
            columns={"節數": "QTR", "時間": "time_remain"}
        )

    @staticmethod
    def parse_types(x):
        #     template = ['投籃', '籃板', '失誤', '犯規', '抄截', '阻攻', '助攻', '罰球', '替換下', '犯滿離場']
        desc = x["文字直播"]
        events = []

        if "投籃" in desc:

            shot_type = desc[len(x["player"]) : desc.find("投籃")]
            shot_result = False if "不中" in desc else True

            events.append(
                {
                    "player": x["player"],
                    "type": "投籃",
                    "shot_type": shot_type,
                    "shot_result": shot_result,
                }
            )

            if "阻攻" in desc:
                blocked = desc[desc.find("阻攻") + 3 : desc.rfind(" ")]
                events.append({"type": "阻攻", "player": blocked})
            elif "助攻" in desc:
                assisted = desc[desc.find("助攻") + 3 : desc.rfind(" ")]
                events.append({"type": "助攻", "player": assisted})

        elif "助攻" in desc:

            events.append({"type": "助攻", "player": x["player"]})

        elif "籃板" in desc:

            mid = desc[desc.find("(") + 1 : desc.find(")")]
            cumul = {
                "累計" + i[: i.find("籃板")]: int(i[i.find(":") + 1 :])
                for i in mid.split(" ")
            }
            events.append({"type": "籃板", **cumul})

        elif "失誤" in desc:

            events.append({"type": "失誤", "player": x["player"]})

            if "抄截" in desc:
                stolen_by = desc[desc.find("抄截") + 3 : desc.rfind(" ")]
                events.append({"type": "抄截", "player": stolen_by})

        elif "罰球" in desc:

            shot_result = False if "不中" in desc else True
            total_shots = int(desc[desc.find("次罰球") - 1 : desc.find("次罰球")])
            shot_number = int(desc[desc.find("第") + 1 : desc.rfind("罰")])

            events.append(
                {
                    "type": "罰球",
                    "player": x["player"],
                    "shot_result": shot_result,
                    "total_shots": total_shots,
                    "shot_number": shot_number,
                }
            )

        elif "暫停" in desc:

            events.append({"type": "暫停", "team": x["team"]})

        elif "替換下" in desc:

            sub_in, sub_out = desc.split("替換下")

            events.append({"type": "換人", "sub_type": "in", "player": sub_in})
            events.append({"type": "換人", "sub_type": "out", "player": sub_out})

        elif "犯規" in desc:

            events.append({"type": "犯規", "player": x["player"]})

        elif "球權" in desc:

            events.append({"type": "possession", "team": x["team"]})

        elif "比賽結束" in desc:

            events.append({"type": "endgame"})

        else:
            events.append({"type": "unknown", "desc": desc})

        return events


@click.group()
def cli():
    pass


@cli.command()
@click.option("--teams", is_flag=True)
@click.option("--players", is_flag=True)
@click.option("--games", is_flag=True)
@click.option("--events", is_flag=True)
def main(teams, players, games, events):

    with UBA() as site:

        if teams:
            site.fetch_teams()

        if players:
            site.fetch_players()

        if games:
            site.fetch_games()

        if events:
            site.fetch_events()


if __name__ == "__main__":
    main()
