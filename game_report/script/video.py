import click
import ffmpeg
import pendulum
import pysubs2
from loguru import logger

import ujson


class EventGraph:

    def __ini__(self, json_path, video_path):

        self.import_json(json_path)
        self.video_path = video_path

    def import_(self, json_path):

        json = ujson.load(json_path)

        if any([i not in json for i in ['game', 'events']]):
            raise Exception("Doesn't have proper field.")

        else:
            keys = ['name', 'type', 'start', 'duration', 'tags', 'desc']

            for event in self.events:
                if any([i not in event for i in keys]):
                    raise Exception("corrupted event structure.")
                    break
            else:
                self.json = json

    @property
    def game(self):
        return self.json['game']

    @property
    def events(self):
        return self.json['events']

    def transcode(self):
        """Transcode video to mp4 with h264_vaapi."""

        try:
            stream_v = ffmpeg.input(
                self.video_path,
                hwaccel='vaapi',
                vaapi_device='/dev/dri/renderD128',
                hwaccel_output_format='vaapi')

            stream_v = ffmpeg.filter(stream_v, 'format', 'nv12')
            stream_v = ffmpeg.filter(stream_v, 'hwupload')

            stream_v = ffmpeg.filter(stream_v,
                                     'deinterlace_vaapi',
                                     rate='field',
                                     auto='1')

            stream_v = ffmpeg.filter(stream_v, 'fps', '30')
            stream_v = ffmpeg.filter(stream_v,
                                     'scale_vaapi',
                                     w='1280',
                                     h='720',
                                     format="nv12")

            stream_a = ffmpeg.input(self.video_path)['a']

            stream = ffmpeg.output(
                stream_v,
                stream_a,
                "transcode".join(self.video_path.split('.')),
                vcodec='h264_vaapi',
                acodec='aac',
                vb="5M",
                maxrate="5M").overwrite_output()
            stream.run()

        except Exception as e:
            logger.error(e)

    def gen_eventclip(self, video_path, event):

        try:
            if event['type'] == 'clip':

                stream_v = ffmpeg.input(
                    video_path,
                    ss=event['start'],
                    t=event['duration'],
                    hwaccel='vaapi',
                    vaapi_device='/dev/dri/renderD128',
                    hwaccel_output_format='vaapi'
                                        )

                stream_v = ffmpeg.filter(stream_v, 'format', 'nv12')
                stream_v = ffmpeg.filter(stream_v, 'hwupload')

                stream_v = ffmpeg.filter(stream_v,
                                         'deinterlace_vaapi',
                                         rate='field',
                                         auto='1')

                stream_v = ffmpeg.filter(stream_v, 'fps', '30')
                stream_v = ffmpeg.filter(stream_v,
                                         'scale_vaapi',
                                         w='1280',
                                         h='720',
                                         format="nv12")

                stream_a = ffmpeg.input(video_path)['a']

                stream = ffmpeg.output(
                    stream_v,
                    stream_a,
                    "event_name".join(self.video_path.split('.')),
                    vcodec='h264_vaapi',
                    acodec='aac',
                    vb="5M",
                    maxrate="5M").overwrite_output()
                stream.run()

            else:
                stream = ffmpeg.input(
                    video_path,
                    ss=event['start'],
                    hwaccel='vaapi',
                    vaapi_device='/dev/dri/renderD128',
                    hwaccel_output_format='vaapi')

                stream.output(f"/tmp/{event['name']}.png",
                              vframes=1).overwrite_output().run()

                stream = ffmpeg.input(f"/tmp/{event['name']}.png", loop=1)
                stream.output(
                    f"{video_path.split('.')[0]}.mp4", t=event['duration'])
        except Exception as e:
            logger.error(e)

    def toHMS(self, time):
        time = pendulum.from_format(str(time), 'X')
        return time.format('HH:mm:ss.SSS')

    def gen_subtitles(self):

        with open('/tmp/{self.game}_mix.srt', 'w') as writer:
            src = ""

            time_mark = 0
            for idx, event in enumerate(self.events):

                src += f"{idx}\n"

                t_start = self.toHMS(time_mark + event['start'])
                t_end = self.toHMS(
                    time_mark + event['start'] + event['duration'])

                src += f"{t_start} --> {t_end}\n"

                tags = ", ".join([tag for tag in event['tags']]) + "\n"
                src += tags

                src += event['desc'] + '\n'
                src += "\n"

                time_mark += event['duration']

            writer.write(src)


@click.group()
def cli():
    pass


@click.command()
@click.argument('log_path', type=click.Path(exists=True))
@click.argument('video_path', type=click.Path(exists=True))
def gen_mix(log_path, video_path):

    game_log = ujson.load(open(log_path, 'r'))
    with open('/tmp/input.txt', 'w') as writer:
        input_txt = ""
        with open(f"/tmp/merge_{game_log['game']}.srt", 'w') as sub:
            time_mark = 0
            srt_txt = ""
            for idx, event in enumerate(game_log['events']):
                if not event['tags']:
                    line = event['name']
                else:
                    line = f"{event['name']}: {', '.join(event['tags'])}"
                start_time = pendulum.from_format(
                    str(time_mark), 'X').format('HH:mm:ss,SSS')
                end_time = pendulum.from_format(
                    str(time_mark+10), 'X').format('HH:mm:ss,SSS')
                srt_txt += f"{idx+1}\n" + \
                    f"{start_time} --> {end_time}\n" + f"{line}\n"

                if event.get('desc', False):
                    srt_txt += event['desc']
                    srt_txt += "\n"

                srt_txt += '\n'
                time_mark += 10

                stream_v = ffmpeg.input(video_path,
                                        ss=event['start'],
                                        t=event['time'] - event['start'] + 5,
                                        # hwaccel='vaapi',
                                        vaapi_device='/dev/dri/renderD128',
                                        # hwaccel_output_format='vaapi',
                                        )
                stream_a = ffmpeg.input(
                    video_path,
                    ss=event['start'],
                    t=event['time'] - event['start']+5,)['a']

                stream_v = ffmpeg.filter(stream_v, 'format', 'nv12')
                stream_v = ffmpeg.filter(stream_v, 'hwupload')

                stream_v = ffmpeg.filter(
                    stream_v, 'deinterlace_vaapi', rate='field', auto='1')
                stream_v = ffmpeg.filter(stream_v, 'fps', '30')
                stream_v = ffmpeg.filter(stream_v, 'scale_vaapi',
                                         w='1280', h='720', format="nv12")

                stream = ffmpeg.output(stream_v, stream_a,
                                       f"/tmp/{idx}_{game_log['game']}.mp4",
                                       vcodec='h264_vaapi',
                                       acodec='aac',
                                       vb="5M",
                                       maxrate="5M").overwrite_output()
                stream.run()

                input_txt += f"file /tmp/{idx}_{game_log['game']}.mp4\n"

                # ffmpeg.input(video_path, ss=event['start']).output(
                #     f'/tmp/{idx}_{game_log["game"]}_frame.png', vframes=1).overwrite_output().run()
                #
                # ffmpeg.input(f'/tmp/{idx}_{game_log["game"]}_frame.png', loop=1).output(
                #     f'/tmp/{idx}_{game_log["game"]}_loop.mp4', t=2).overwrite_output().run()
                #
                # input_txt += f"file /tmp/{idx}_{game_log['game']}_loop.mp4\n"

            sub.write(srt_txt)

        writer.write(input_txt)

    subs = pysubs2.load(f"/tmp/merge_{game_log['game']}.srt")
    subs.save(f"/tmp/merge_{game_log['game']}.ass")
    ffmpeg.input('/tmp/input.txt', format='concat',
                 safe=0).output(f"/tmp/merge_{game_log['game']}.mp4", c='copy', movflags="faststart").overwrite_output().run()


cli.add_command(gen_mix)

if __name__ == "__main__":
    cli()
