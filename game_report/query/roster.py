from game_report.database import db


async def get_roster():

    col = db.teams

    pipeline = [
        {
            "$lookup": {
                "from": "players",
                "let": {"team": "$_id"},
                "pipeline": [
                    {"$match": {"$expr": {"$eq": ["$team", "$$team"]}}},
                    {"$project": {"_id": 0, "player": 1, "jersey": 1}},
                    {"$sort": {{"$"}: 1}},
                ],
                "as": "players",
            }
        },
        {"$project": {"_id": 0}},
    ]

    docs = [i async for i in col.aggregate(pipeline)]
    return docs


async def get_rotation():

    col = db.events

    pipeline = [
        {"$match": {"$expr": {"$in": ["換人", "$events.type"]}}},
        {"$unwind": "$events"},
        {
            "$group": {
                "_id": {
                    "team": "$team",
                    "game": "$game",
                    "seconds": "$seconds",
                    "type": "$events.sub_type",
                },
                "subs": {"$push": "$events.player"},
            }
        },
        {
            "$group": {
                "_id": {
                    "team": "$_id.team",
                    "game": "$_id.game",
                    "seconds": "$_id.seconds",
                },
                "sub_logs": {"$push": {"k": "$_id.type", "v": "$subs"}},
            }
        },
        {"$addFields": {"sub_logs": {"$arrayToObject": "$sub_logs"}}},
        {"$sort": {"_id.game": 1, "_id.team": 1, "_id.seconds": 1}},
        {
            "$group": {
                "_id": {"team": "$_id.team", "game": "$_id.game"},
                "sub_logs": {"$push": {"seconds": "$_id.seconds", "subs": "$sub_logs"}},
            }
        },
        {
            "$lookup": {
                "from": "games",
                "let": {"game": "$_id.game", "team": "$_id.team"},
                "pipeline": [
                    {"$unwind": "$box"},
                    {
                        "$match": {
                            "$expr": {
                                "$eq": [
                                    ["$_id", "$box.team", "$box.先發"],
                                    ["$$game", "$$team", True],
                                ]
                            }
                        }
                    },
                    {"$project": {"player": "$box.player"}},
                    {"$project": {"_id": 0}},
                ],
                "as": "starters",
            }
        },
        {
            "$addFields": {
                "rotational_log": {
                    "$reduce": {
                        "input": "$sub_logs",
                        "initialValue": ["$starters.player"],
                        "in": {
                            "$concatArrays": [
                                "$$value",
                                [
                                    {
                                        "$setUnion": [
                                            "$$this.subs.in",
                                            {
                                                "$setDifference": [
                                                    {
                                                        "$arrayElemAt": [
                                                            {
                                                                "$reverseArray": "$$value"
                                                            },
                                                            0,
                                                        ]
                                                    },
                                                    "$$this.subs.out",
                                                ]
                                            },
                                        ]
                                    }
                                ],
                            ]
                        },
                    }
                }
            }
        },
        {"$project": {"starters": 0}},
        {
            "$lookup": {
                "from": "events",
                "let": {"game": "$_id.game"},
                "pipeline": [
                    {
                        "$match": {
                            "$expr": {
                                "$and": [
                                    {"$eq": ["$game", "$$game"]},
                                    {"$in": ["endgame", "$events.type"]},
                                ]
                            }
                        }
                    },
                    {"$project": {"seconds": 1, "_id": 0}},
                ],
                "as": "endtime",
            }
        },
        {
            "$addFields": {
                "time_range": {
                    "$zip": {
                        "inputs": [
                            {"$concatArrays": [[0.0], "$sub_logs.seconds"]},
                            {
                                "$concatArrays": [
                                    "$sub_logs.seconds",
                                    "$endtime.seconds",
                                ]
                            },
                        ]
                    }
                }
            }
        },
        {"$project": {"endtime": 0, "sub_logs": 0}},
        {
            "$addFields": {
                "rotational_log": {
                    "$map": {
                        "input": {
                            "$zip": {"inputs": ["$rotational_log", "$time_range"]}
                        },
                        "in": {
                            "$arrayToObject": {
                                "$zip": {"inputs": [["rotation", "session"], "$$this"]}
                            }
                        },
                    }
                }
            }
        },
        {"$project": {"time_range": 0}},
        {"$unwind": "$rotational_log"},
        {
            "$group": {
                "_id": {"rotation": "$rotational_log.rotation", "team": "$_id.team"},
                "sessions": {
                    "$push": {"game": "$_id.game", "session": "$rotational_log.session"}
                },
            }
        },
        {
            "$group": {
                "_id": {"rotation": "$_id.rotation", "team": "$_id.team"},
                "sessions": {"$push": {"game": "$_id.game", "session": "$sessions"}},
            }
        },
        {
            "$group": {
                "_id": "$_id.team",
                "rotations": {
                    "$push": {"lineup": "$_id.rotation", "session": "$sessions"}
                },
            }
        },
    ]

    docs = [i async for i in col.aggregate(pipeline)]
    return docs
