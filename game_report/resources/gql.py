import graphene
import numpy as np
from graphql.execution.executors.asyncio import AsyncioExecutor
from loguru import logger
from starlette.graphql import GraphQLApp

from game_report.database import db
from game_report.gql_query import info as ginfo


class Container(graphene.ObjectType):

    player = graphene.String()
    avg = graphene.Float()
    gp = graphene.Int()
    pts = graphene.Int()

    def resolve_avg(self, info):
        return round(self.avg, 2)


class PlayerAnalyze(graphene.ObjectType):

    avg_pts = graphene.List(Container, players=graphene.List(graphene.String))
    avg_reb = graphene.Float()
    avg_ast = graphene.Float()

    async def resolve_avg_pts(self, info, players=None):
        col, pipeline = self

        pipeline += [
            {
                "$match": {
                    "$expr": {
                        "$and": [
                            {} if not players else {"$in": ["$player", players]},
                            {"$in": ["$events.type", ["投籃", "罰球"]]},
                            {"$eq": ["$events.shot_result", True]},
                        ]
                    }
                }
            },
            {
                "$group": {
                    "_id": {"player": "$player", "team": "$team"},
                    "pts": {
                        "$sum": {
                            "$switch": {
                                "branches": [
                                    {
                                        "case": {"$eq": ["$events.shot_type", "2分"]},
                                        "then": 2,
                                    },
                                    {
                                        "case": {"$eq": ["$events.shot_type", "3分"]},
                                        "then": 3,
                                    },
                                    {
                                        "case": {"$eq": ["$events.type", "罰球"]},
                                        "then": 1,
                                    },
                                ],
                                "default": 0,
                            }
                        }
                    },
                }
            },
            {
                "$lookup": {
                    "from": "games",
                    "let": {"player": "$_id.player", "team": "$_id.team"},
                    "pipeline": [
                        {"$unwind": "$box"},
                        {
                            "$match": {
                                "$expr": {
                                    "$and": [
                                        {"$eq": ["$box.team", "$$team"]},
                                        {"$eq": ["$box.player", "$$player"]},
                                    ]
                                }
                            }
                        },
                        {"$match": {"$expr": {"$gt": ["$box.時間", 0.0]}}},
                        {"$count": "games_played"},
                    ],
                    "as": "result",
                }
            },
            {"$unwind": "$result"},
            {"$addFields": {"player": "$_id.player", "gp": "$result.games_played"}},
            {"$addFields": {"avg": {"$divide": ["$pts", "$gp"]}}},
            {"$project": {"_id": 0, "player": 1, "avg": 1, "gp": 1, "pts": 1}},
        ]

        return [Container(**i) async for i in col.aggregate(pipeline)]


class Query(graphene.ObjectType):
    teams = graphene.List(ginfo.Team, team=graphene.String())
    games = graphene.List(ginfo.Game, team=graphene.String())

    async def resolve_teams(self, info, team=None):

        return [
            ginfo.Team(**i)
            async for i in db.teams.find(
                {} if not team else {"team": team}, projection={"team": 1, "_id": 1}
            )
        ]

    async def resolve_games(self, info, team=None):
        pipeline = []
        if team is not None:
            pipeline += [{"$match": {"$expr": {"$in": [team, "$result.team"]}}}]

        pipeline += [
            {"$addFields": {"teams": "$result.team"}},
            {"$project": {"game": 1, "teams": 1}},
        ]

        return [ginfo.Game(**i) async for i in db.games.aggregate(pipeline)]


app = GraphQLApp(schema=graphene.Schema(query=Query), executor_class=AsyncioExecutor)
