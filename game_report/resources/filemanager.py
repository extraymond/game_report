import asyncio
from io import StringIO
from pathlib import Path

from starlette.exceptions import HTTPException
from starlette.responses import FileResponse, StreamingResponse, UJSONResponse
from starlette.routing import Router

from game_report import util
from game_report.database import db

path = Path("/media/storage/Videos/成大校女籃/大專盃")


app = Router()


@app.route('/files')
async def list_files(req):

    files = [{'folder': folder.absolute().__str__(), 'files': [i.absolute().__str__(
    ) for i in folder.iterdir()]} for folder in path.iterdir() if folder.is_dir()]

    return UJSONResponse(files)


@app.route('/video')
async def get_video(req):

    body = await req.body()
    if body:
        json = await req.json()

        video = json.get('video', None)

        if not video:
            raise HTTPException(404)

        else:
            print(video)
            path = Path(video)

            return FileResponse(path.absolute().__str__())
    else:
        return FileResponse("/media/storage/Videos/成大校女籃/大專盃/台師台科/fullv.mp4")
