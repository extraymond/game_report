from starlette.responses import StreamingResponse
from starlette.routing import Router

from game_report import util
from game_report.database import db

app = Router()


@app.route('/teams')
async def teams(req):
    cur = db.teams.find({}, projection={'_id': 1, 'team': 1})
    return StreamingResponse(util.stream_json(cur),
                             media_type='application/json')


@app.route('/games')
async def games(req):
    team_val = req.query_params.get('team', None)
    if not team_val:
        cur = db.games.find({}, projection={'result.team': 1})
    else:
        cur = db.games.aggregate([
            {'$match': {'result.team': team_val}},
            {'$project': {'result': 1}},
            {'$unwind': '$result'},
            {'$match': {'$expr': {'$ne': ['$result.team', team_val]}}},
            {'$project': {'against': '$result.team'}}
        ])

    return StreamingResponse(util.stream_json(cur),
                             media_type='application/json')
