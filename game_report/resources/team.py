from loguru import logger
from starlette.exceptions import HTTPException
from starlette.responses import StreamingResponse, UJSONResponse
from starlette.routing import Router

from game_report import util
from game_report.database import db
from game_report.query import roster

app = Router()


@app.route("/roster")
async def team_roster(req):

    docs = await roster.get_roster()
    return UJSONResponse(docs)


@app.route("/rotation")
async def team_rotation(req):

    docs = await roster.get_rotation()
    return UJSONResponse(docs)


# @app.route("/rotation")
async def rotation(req):
    team = req.query_params.get("team", None)
    game = req.query_params.get("game", None)
    player = req.query_params.get("player", None)

    logger.info(team)

    cur = db.events.aggregate(
        [
            {
                "$match": {
                    "$expr": {
                        "$and": [
                            True if not team else {"$eq": ["$team", team]},
                            True if not game else {"$eq": ["$game", game]},
                            {"$in": ["換人", "$events.type"]},
                        ]
                    }
                }
            },
            {"$unwind": "$events"},
            {"$sort": {"team": -1, "game": 1, " seconds": 1}},
            {
                "$group": {
                    "_id": {"game": "$game", "team": "$team", "seconds": "$seconds"},
                    "subs": {
                        "$push": {
                            "type": "$events.sub_type",
                            "player": "$events.player",
                        }
                    },
                }
            },
            {"$sort": {"_id.team": 1, "_id.game": 1, "_id.seconds": 1}},
            {
                "$group": {
                    "_id": {"team": "$_id.team", "game": "$_id.game"},
                    "logs": {"$push": {"seconds": "$_id.seconds", "subs": "$subs"}},
                }
            },
            {
                "$lookup": {
                    "from": "games",
                    "let": {"game": "$_id.game", "team": "$_id.team"},
                    "pipeline": [
                        {"$match": {"$expr": {"$eq": ["$_id", "$$game"]}}},
                        {"$unwind": "$box"},
                        {
                            "$match": {
                                "$expr": {
                                    "$and": [
                                        {"$eq": ["$box.team", "$$team"]},
                                        "$box.先發",
                                    ]
                                }
                            }
                        },
                        {
                            "$group": {
                                "_id": None,
                                "result": {
                                    "$push": {"type": "in", "player": "$box.player"}
                                },
                            }
                        },
                        {"$project": {"_id": 0}},
                    ],
                    "as": "starters",
                }
            },
            {"$unwind": "$starters"},
            {
                "$addFields": {
                    "logs": {
                        "$concatArrays": [
                            [{"seconds": 0.0, "subs": "$starters.result"}],
                            "$logs",
                        ]
                    }
                }
            },
            {
                "$addFields": {
                    "cum_log": {
                        "$reduce": {
                            "input": "$logs",
                            "initialValue": {"pools": [], "outs": []},
                            "in": {
                                "pools": {
                                    "$concatArrays": ["$$value.pools", "$$this.subs"]
                                },
                                "outs": {
                                    "$concatArrays": [
                                        "$$value.outs",
                                        [
                                            {
                                                "seconds": "$$this.seconds",
                                                "cum_subs": {
                                                    "$concatArrays": [
                                                        "$$value.pools",
                                                        "$$this.subs",
                                                    ]
                                                },
                                            }
                                        ],
                                    ]
                                },
                            },
                        }
                    }
                }
            },
            {"$addFields": {"cum_log": "$cum_log.outs"}},
            {"$project": {"logs": 0}},
            {"$unwind": {"path": "$cum_log", "includeArrayIndex": "idx_log"}},
            {"$unwind": "$cum_log.cum_subs"},
            {
                "$group": {
                    "_id": {
                        "team": "$_id.team",
                        "game": "$_id.game",
                        "seconds": "$cum_log.seconds",
                        "player": "$cum_log.cum_subs.player",
                        "type": "$cum_log.cum_subs.type",
                    },
                    "records": {"$sum": 1},
                }
            },
            {
                "$group": {
                    "_id": {
                        "team": "$_id.team",
                        "seconds": "$_id.seconds",
                        "player": "$_id.player",
                        "game": "$_id.game",
                    },
                    "on_court": {"$push": {"k": "$_id.type", "v": "$records"}},
                }
            },
            {
                "$addFields": {
                    "on_court": {
                        "$mergeObjects": [
                            {"in": 0, "out": 0},
                            {"$arrayToObject": "$on_court"},
                        ]
                    }
                }
            },
            {"$addFields": {"on_court": {"$gt": ["$on_court.in", "$on_court.out"]}}},
            {"$match": {"on_court": True}},
            #
            {
                "$group": {
                    "_id": {
                        "team": "$_id.team",
                        "seconds": "$_id.seconds",
                        "game": "$_id.game",
                    },
                    "lineup": {"$push": "$_id.player"},
                }
            },
            {"$match": {"$expr": {"$eq": [5, {"$size": "$lineup"}]}}},
            {"$match": {} if not player else {"$expr": {"$in": [player, "$lineup"]}}},
            {"$sort": {"_id.game": 1, "_id.team": 1, "_id.seconds": 1}},
            {
                "$group": {
                    "_id": {"team": "$_id.team", "game": "$_id.game"},
                    "lineups": {"$push": {"time": "$_id.seconds", "lineup": "$lineup"}},
                }
            },
            {
                "$lookup": {
                    "from": "games",
                    "let": {"game": "$_id.game", "team": "$_id.team"},
                    "pipeline": [
                        {"$unwind": "$result"},
                        {
                            "$match": {
                                "$expr": {
                                    "$and": [
                                        {"$eq": ["$_id", "$$game"]},
                                        {"$eq": ["$result.team", "$$team"]},
                                    ]
                                }
                            }
                        },
                        {
                            "$addFields": {
                                "endtime": {
                                    "$let": {
                                        "vars": {
                                            "extras": {
                                                "$subtract": [
                                                    {
                                                        "$size": {
                                                            "$objectToArray": "$result"
                                                        }
                                                    },
                                                    6,
                                                ]
                                            }
                                        },
                                        "in": {
                                            "$add": [
                                                40 * 60,
                                                {"$multiply": [5 * 60, "$$extras"]},
                                            ]
                                        },
                                    }
                                }
                            }
                        },
                        {"$project": {"endtime": 1}},
                    ],
                    "as": "endtime",
                }
            },
            {"$unwind": "$endtime"},
            {"$addFields": {"endtime": "$endtime.endtime"}},
            {
                "$addFields": {
                    "lineups": {
                        "$reduce": {
                            "input": {"$reverseArray": "$lineups"},
                            "initialValue": {"next": "$endtime", "outs": []},
                            "in": {
                                "next": "$$this.time",
                                "outs": {
                                    "$concatArrays": [
                                        "$$value.outs",
                                        [
                                            {
                                                "lineup": "$$this.lineup",
                                                "in": "$$this.time",
                                                "out": "$$value.next",
                                            }
                                        ],
                                    ]
                                },
                            },
                        }
                    }
                }
            },
            {"$project": {"endtime": 0}},
            {"$addFields": {"lineups": {"$reverseArray": "$lineups.outs"}}},
            {"$unwind": "$lineups"},
            {
                "$addFields": {
                    "lineups.session": {"$subtract": ["$lineups.out", "$lineups.in"]}
                }
            },
            {"$unwind": "$lineups.lineup"},
            {"$sort": {"lineups.in": 1, "lineups.lineup": 1}},
            {
                "$group": {
                    "_id": {
                        "game": "$_id.game",
                        "team": "$_id.team",
                        "in": "$lineups.in",
                        "out": "$lineups.out",
                        "session": "$lineups.session",
                    },
                    "lineup": {"$push": "$lineups.lineup"},
                }
            },
            {"$sort": {"_id.team": 1, "_id.game": 1, "_id.in": 1}},
            {
                "$group": {
                    "_id": {"team": "$_id.team", "lineup": "$lineup"},
                    "sessions": {"$push": "$_id"},
                }
            },
            {"$addFields": {"court_time": {"$sum": "$sessions.session"}}},
            {"$sort": {"_id.team": 1, "court_time": -1}},
            {
                "$group": {
                    "_id": {"team": "$_id.team"},
                    "lineups": {
                        "$push": {"lineup": "$_id.lineup", "court_time": "$court_time"}
                    },
                }
            },
        ]
    )

    return StreamingResponse(util.stream_json(cur), media_type="application/json")


@app.route("/lineup")
async def lineup_events(req):

    body = await req.body()
    if not body:
        raise HTTPException(404)
    else:
        json = await req.json()
        team = json.get("team", None)
        game = json.get("game", None)
        lineup = json.get("lineup", None)

        cur = db.events.aggregate(
            [
                {
                    "$match": {
                        "$expr": {
                            "$and": [
                                True if not team else {"$eq": ["$team", team]},
                                True if not game else {"$eq": ["$game", game]},
                                {"$in": ["換人", "$events.type"]},
                            ]
                        }
                    }
                },
                {"$unwind": "$events"},
                {"$sort": {"team": -1, "game": 1, " seconds": 1}},
                {
                    "$group": {
                        "_id": {
                            "game": "$game",
                            "team": "$team",
                            "seconds": "$seconds",
                        },
                        "subs": {
                            "$push": {
                                "type": "$events.sub_type",
                                "player": "$events.player",
                            }
                        },
                    }
                },
                {"$sort": {"_id.team": 1, "_id.game": 1, "_id.seconds": 1}},
                {
                    "$group": {
                        "_id": {"team": "$_id.team", "game": "$_id.game"},
                        "logs": {"$push": {"seconds": "$_id.seconds", "subs": "$subs"}},
                    }
                },
                {
                    "$lookup": {
                        "from": "games",
                        "let": {"game": "$_id.game", "team": "$_id.team"},
                        "pipeline": [
                            {"$match": {"$expr": {"$eq": ["$_id", "$$game"]}}},
                            {"$unwind": "$box"},
                            {
                                "$match": {
                                    "$expr": {
                                        "$and": [
                                            {"$eq": ["$box.team", "$$team"]},
                                            "$box.先發",
                                        ]
                                    }
                                }
                            },
                            {
                                "$group": {
                                    "_id": None,
                                    "result": {
                                        "$push": {"type": "in", "player": "$box.player"}
                                    },
                                }
                            },
                            {"$project": {"_id": 0}},
                        ],
                        "as": "starters",
                    }
                },
                {"$unwind": "$starters"},
                {
                    "$addFields": {
                        "logs": {
                            "$concatArrays": [
                                [{"seconds": 0.0, "subs": "$starters.result"}],
                                "$logs",
                            ]
                        }
                    }
                },
                {
                    "$addFields": {
                        "cum_log": {
                            "$reduce": {
                                "input": "$logs",
                                "initialValue": {"pools": [], "outs": []},
                                "in": {
                                    "pools": {
                                        "$concatArrays": [
                                            "$$value.pools",
                                            "$$this.subs",
                                        ]
                                    },
                                    "outs": {
                                        "$concatArrays": [
                                            "$$value.outs",
                                            [
                                                {
                                                    "seconds": "$$this.seconds",
                                                    "cum_subs": {
                                                        "$concatArrays": [
                                                            "$$value.pools",
                                                            "$$this.subs",
                                                        ]
                                                    },
                                                }
                                            ],
                                        ]
                                    },
                                },
                            }
                        }
                    }
                },
                {"$addFields": {"cum_log": "$cum_log.outs"}},
                {"$project": {"logs": 0}},
                {"$unwind": {"path": "$cum_log", "includeArrayIndex": "idx_log"}},
                {"$unwind": "$cum_log.cum_subs"},
                {
                    "$group": {
                        "_id": {
                            "team": "$_id.team",
                            "game": "$_id.game",
                            "seconds": "$cum_log.seconds",
                            "player": "$cum_log.cum_subs.player",
                            "type": "$cum_log.cum_subs.type",
                        },
                        "records": {"$sum": 1},
                    }
                },
                {
                    "$group": {
                        "_id": {
                            "team": "$_id.team",
                            "seconds": "$_id.seconds",
                            "player": "$_id.player",
                            "game": "$_id.game",
                        },
                        "on_court": {"$push": {"k": "$_id.type", "v": "$records"}},
                    }
                },
                {
                    "$addFields": {
                        "on_court": {
                            "$mergeObjects": [
                                {"in": 0, "out": 0},
                                {"$arrayToObject": "$on_court"},
                            ]
                        }
                    }
                },
                {
                    "$addFields": {
                        "on_court": {"$gt": ["$on_court.in", "$on_court.out"]}
                    }
                },
                {"$match": {"on_court": True}},
                {
                    "$group": {
                        "_id": {
                            "team": "$_id.team",
                            "seconds": "$_id.seconds",
                            "game": "$_id.game",
                        },
                        "lineup": {"$push": "$_id.player"},
                    }
                },
                {"$match": {"$expr": {"$eq": [5, {"$size": "$lineup"}]}}},
                {
                    "$match": {}
                    if not lineup
                    else {"$expr": {"$setEquals": [lineup, "$lineup"]}}
                },
                {"$sort": {"_id.game": 1, "_id.team": 1, "_id.seconds": 1}},
                {
                    "$group": {
                        "_id": {"team": "$_id.team", "game": "$_id.game"},
                        "lineups": {
                            "$push": {"time": "$_id.seconds", "lineup": "$lineup"}
                        },
                    }
                },
                {
                    "$lookup": {
                        "from": "games",
                        "let": {"game": "$_id.game", "team": "$_id.team"},
                        "pipeline": [
                            {"$unwind": "$result"},
                            {
                                "$match": {
                                    "$expr": {
                                        "$and": [
                                            {"$eq": ["$_id", "$$game"]},
                                            {"$eq": ["$result.team", "$$team"]},
                                        ]
                                    }
                                }
                            },
                            {
                                "$addFields": {
                                    "endtime": {
                                        "$let": {
                                            "vars": {
                                                "extras": {
                                                    "$subtract": [
                                                        {
                                                            "$size": {
                                                                "$objectToArray": "$result"
                                                            }
                                                        },
                                                        6,
                                                    ]
                                                }
                                            },
                                            "in": {
                                                "$add": [
                                                    40 * 60,
                                                    {"$multiply": [5 * 60, "$$extras"]},
                                                ]
                                            },
                                        }
                                    }
                                }
                            },
                            {"$project": {"endtime": 1}},
                        ],
                        "as": "endtime",
                    }
                },
                {"$unwind": "$endtime"},
                {"$addFields": {"endtime": "$endtime.endtime"}},
                {
                    "$addFields": {
                        "lineups": {
                            "$reduce": {
                                "input": {"$reverseArray": "$lineups"},
                                "initialValue": {"next": "$endtime", "outs": []},
                                "in": {
                                    "next": "$$this.time",
                                    "outs": {
                                        "$concatArrays": [
                                            "$$value.outs",
                                            [
                                                {
                                                    "lineup": "$$this.lineup",
                                                    "in": "$$this.time",
                                                    "out": "$$value.next",
                                                }
                                            ],
                                        ]
                                    },
                                },
                            }
                        }
                    }
                },
                {"$project": {"endtime": 0}},
                {"$addFields": {"lineups": {"$reverseArray": "$lineups.outs"}}},
                {"$unwind": "$lineups"},
                {
                    "$addFields": {
                        "lineups.session": {
                            "$subtract": ["$lineups.out", "$lineups.in"]
                        }
                    }
                },
                {"$unwind": "$lineups.lineup"},
                {"$sort": {"lineups.in": 1, "lineups.lineup": 1}},
                {
                    "$group": {
                        "_id": {
                            "game": "$_id.game",
                            "team": "$_id.team",
                            "in": "$lineups.in",
                            "out": "$lineups.out",
                            "session": "$lineups.session",
                        },
                        "lineup": {"$push": "$lineups.lineup"},
                    }
                },
                {"$sort": {"_id.team": 1, "_id.game": 1, "_id.in": 1}},
                {
                    "$group": {
                        "_id": {"team": "$_id.team", "lineup": "$lineup"},
                        "sessions": {"$push": "$_id"},
                    }
                },
            ]
        )

        return StreamingResponse(util.stream_json(cur), media_type="application/json")


@app.route("/sessions")
async def session_performance(req):

    body = await req.body()
    if not body:
        raise HTTPException(404)
    else:
        json = await req.json()
        team = json.get("team", None)
        game = json.get("game", None)
        time = json.get("session", None)

        cur = db.events.aggregate(
            [
                {
                    "$match": {
                        "$expr": {
                            "$and": [True if not game else {"$eq": ["$game", game]}]
                        }
                    }
                },
                {
                    "$match": {}
                    if not time
                    else {"seconds": {"$gte": time[0], "$lt": time[1]}}
                },
                {"$unwind": "$events"},
                {
                    "$lookup": {
                        "from": "games",
                        "localField": "game",
                        "foreignField": "_id",
                        "as": "game_info",
                    }
                },
                {"$unwind": "$game_info"},
                {
                    "$match": {}
                    if not team
                    else {"$expr": {"$in": [team, "$game_info.result.team"]}}
                },
                {
                    "$addFields": {
                        "opp": {
                            "$arrayElemAt": [
                                {
                                    "$filter": {
                                        "input": "$game_info.result.team",
                                        "cond": {"$ne": ["$team", "$$this"]},
                                    }
                                },
                                0,
                            ]
                        }
                    }
                },
                {
                    "$addFields": {
                        "possession": {
                            "$switch": {
                                "branches": [
                                    {
                                        "case": {
                                            "$and": [
                                                {"$eq": ["$events.type", "投籃"]},
                                                {"$eq": ["$events.shot_result", True]},
                                            ]
                                        },
                                        "then": [
                                            {"type": "gain", "by": "$opp"},
                                            {"type": "loss", "by": "$team"},
                                        ],
                                    },
                                    {
                                        "case": {
                                            "$and": [
                                                {"$eq": ["$events.type", "罰球"]},
                                                {
                                                    "$eq": [
                                                        "$events.total_shots",
                                                        "$events.shot_number",
                                                    ]
                                                },
                                            ]
                                        },
                                        "then": [
                                            {"type": "gain", "by": "$opp"},
                                            {"type": "loss", "by": "$team"},
                                        ],
                                    },
                                    {
                                        "case": {"$eq": ["防守籃板", "$events.reb_type"]},
                                        "then": [
                                            {"type": "gain", "by": "$team"},
                                            {"type": "loss", "by": "$opp"},
                                        ],
                                    },
                                    {
                                        "case": {"$eq": ["$events.type", "失誤"]},
                                        "then": [
                                            {"type": "gain", "by": "$opp"},
                                            {"type": "loss", "by": "$team"},
                                        ],
                                    },
                                ],
                                "default": None,
                            }
                        }
                    }
                },
                {"$project": {"game_info": 0}},
                {"$unwind": "$possession"},
                {"$sort": {"game": 1, "seconds": 1}},
                {"$match": {"possession.type": "loss"}},
                {
                    "$group": {
                        "_id": {"game": "$game", "team": "$possession.by"},
                        "possessions": {"$sum": 1},
                    }
                },
                {
                    "$lookup": {
                        "from": "games",
                        "let": {"game": "$_id.game", "team": "$_id.team"},
                        "pipeline": [
                            {"$unwind": "$result"},
                            {
                                "$match": {
                                    "$expr": {
                                        "$and": [
                                            {"$eq": ["$_id", "$$game"]},
                                            {"$eq": ["$result.team", "$$team"]},
                                        ]
                                    }
                                }
                            },
                            {
                                "$addFields": {
                                    "endtime": {
                                        "$let": {
                                            "vars": {
                                                "extras": {
                                                    "$subtract": [
                                                        {
                                                            "$size": {
                                                                "$objectToArray": "$result"
                                                            }
                                                        },
                                                        6,
                                                    ]
                                                }
                                            },
                                            "in": {
                                                "$add": [
                                                    40 * 60,
                                                    {"$multiply": [5 * 60, "$$extras"]},
                                                ]
                                            },
                                        }
                                    }
                                }
                            },
                            {"$project": {"endtime": 1}},
                        ],
                        "as": "endtime",
                    }
                },
                {"$unwind": "$endtime"},
                {
                    "$addFields": {
                        "mp": "$endtime.endtime" if not time else time[1] - time[0]
                    }
                },
                {"$project": {"endtime": 0}},
                {
                    "$addFields": {
                        "pace": {
                            "$divide": [
                                {
                                    "$trunc": {
                                        "$multiply": [
                                            {"$divide": ["$possessions", "$mp"]},
                                            10000,
                                        ]
                                    }
                                },
                                100,
                            ]
                        }
                    }
                },
                {"$sort": {"_id.game": 1, "pace": -1}},
                {
                    "$group": {
                        "_id": "$_id.game",
                        "teams": {
                            "$push": {
                                "team": "$_id.team",
                                "possessions": "$possessions",
                                "pace": "$pace",
                            }
                        },
                    }
                },
            ]
        )

        return StreamingResponse(util.stream_json(cur), media_type="application/json")
