import abc
from pprint import pprint

from pymongo import MongoClient

client = MongoClient()
db = client['games_report']


class Graph(abc.ABC):
    """build pipeline graphs."""

    def __init__(self, col, team=None, game=None, player=None, time_span=None):
        self.col = col
        self.team = team
        self.game = game
        self.player = player
        self.time_span = time_span

    @abc.abstractmethod
    def draft(self):
        pass

    def invoke(self):
        cur = self.col.aggregate(self.pipelines)
        return list(cur)


def roster():
    """show the lineup changs for a team in a game."""

    cor = db.games.aggregate([
        {'$match': {'events': True}},

        # find roster of this game.
        {'$project': {'teams': 1, 'box.player': 1, 'box.先發': 1, 'box.team': 1}},
        {'$project': {'players': '$box'}},

        {'$addFields': {'endtime': {'$switch': {
            'branches': [
                {'case': {'$ne': [{'$type': '$result.Q3'},
                                  'missing']}, 'then': 3300.0},
                {'case': {'$ne': [{'$type': '$result.Q2'},
                                  'missing']}, 'then': 3000.0},
                {'case': {'$ne': [{'$type': '$result.Q1'},
                                  'missing']}, 'then': 2700.0},
                ],
            'default': 2400.0
        }}}
         },


        # search for substitutions changes.
        {'$lookup': {
            'from': 'events',
            'let': {'game': '$_id', 'rosters': '$players', 'endtime': '$endtime'},
            'pipeline': [
                {'$match': {'$expr': {'$and': [
                    {'$eq': ['$game', '$$game']},
                    {'$in': ['換人', '$events.type', ]}
                    ]}}},
                {'$unwind': '$events'},
                {'$group': {'_id': {'team': '$team', 'seconds': '$seconds'},
                            'subs': {'$push': {'type': '$events.sub_type',
                                               'player': '$events.player'}}
                            },
                 },
                {'$sort': {'_id.team': 1, '_id.seconds': 1}},
                {'$group': {'_id': '$_id.team', 'logs': {
                    '$push': {'seconds': '$_id.seconds', 'subs': '$subs'}}}},
                {'$addFields': {'logs': {'$concatArrays': [[{
                            'seconds': 0.0,
                            'subs': {'$map': {
                                'input': {'$filter': {
                                    'input': '$$rosters',
                                    'cond': {'$eq': [
                                        ['$$this.先發', '$$this.team'],
                                        [True, '$_id']]}}},
                                'in': {'player': '$$this.player', 'type': 'in'}
                                }},
                            }], '$logs']
                    }}},
                {'$addFields': {'cum_log': {'$reduce': {
                    'input': '$logs',
                    'initialValue': {'pools': [], 'outs': []},
                    'in': {
                        'pools': {'$concatArrays': ['$$value.pools', '$$this.subs']}, 'outs': {'$concatArrays': [
                            '$$value.outs',
                            [{'seconds': '$$this.seconds', 'cum_subs': {
                                '$concatArrays': ['$$value.pools', '$$this.subs']}}]
                            ]}}
                    }}}},
                {'$addFields': {'cum_log': '$cum_log.outs'}},
                {'$project': {'logs': 0}},
                {'$unwind': {'path': '$cum_log', 'includeArrayIndex': 'idx_log'}},
                {'$unwind': '$cum_log.cum_subs'},

                {'$group': {'_id': {
                    'team': '$_id',
                    'seconds': '$cum_log.seconds',
                    'player': '$cum_log.cum_subs.player',
                    'type': '$cum_log.cum_subs.type'
                    },
                    'records': {'$sum': 1}
                    }},
                {'$group': {'_id': {
                    'team': '$_id.team',
                    'seconds': '$_id.seconds',
                    'player': '$_id.player',
                    },
                    'on_court': {'$push': {'k': '$_id.type', 'v': '$records'}}
                    }},
                {'$addFields': {'on_court': {'$mergeObjects': [
                    {'in': 0, 'out': 0},
                    {'$arrayToObject': '$on_court'}
                    ]}}
                 },
                {'$addFields': {'on_court': {
                    '$gt': ['$on_court.in', '$on_court.out']}}},
                {'$match': {'on_court': True}},
                {'$sort': {'_id.seconds': 1}},

                {'$group': {'_id': {'team': '$_id.team', 'seconds': '$_id.seconds'},
                            'lineup': {'$push': '$_id.player'}}
                 },
                {'$sort': {'team': 1, '_id.seconds': 1}},
                {'$group': {
                     '_id': '$_id.team',
                     'lineups': {'$push': {
                         'time': '$_id.seconds',
                         'lineup': '$lineup'}
                         }
                 }},
                {'$addFields': {'lineups': {'$reduce': {
                    'input': {'$reverseArray': '$lineups'},
                    'initialValue': {'next': '$$endtime', 'outs': []},
                    'in': {
                        'next': '$$this.time',
                        'outs': {'$concatArrays': ['$$value.outs', [{
                            'lineup': '$$this.lineup',
                            'in': '$$this.time',
                            'out': '$$value.next'}
                        ]]}}
                }}}},
                {'$addFields': {'lineups': {'$reverseArray': '$lineups.outs'}}},

                ],
            'as': 'subs'
            }},
        {'$project': {'players': 0}}
        ])

    doc = list(cor)
    pprint(doc)


class ScoringGraph(Graph):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @property
    def pre_select(self):
        addition = {}
        if self.game is not None:
            addition['_id'] = self.game
        return [{'$match': {'events': True, **addition}}]

    @property
    def pipelines(self):
        return self.pre_select + self.events_lookup + self.post_cleanup

    @property
    def lookup_matcher(self):
        match = {}
        for i, j in zip(
                ['team', 'player', 'time_span'],
                [self.team, self.player, self.time_span]):
            if j is not None:
                match[i] = j
        if not match:
            return {'$match': {'$expr': {'$eq': ['$game', '$$game']}}}
        else:
            return {'$match': match}

    @property
    def events_lookup(self):
        return [{
            '$lookup': {
                'from': 'events',
                'let': {'game': '$_id', },
                'pipeline': [
                    self.lookup_matcher,
                    {'$unwind': '$events'},
                    {'$match': {'$expr': {'$eq': ['投籃', '$events.type']}}},
                    {'$group': {'_id': {
                        'team': '$team',
                        'shot_type': '$events.shot_type',
                    },
                        'attemps': {'$sum': 1},
                        'made': {'$sum': {'$cond': {
                            'if':  '$events.shot_result',
                            'then': 1,
                            'else': 0}}},
                        'miss': {'$sum': {'$cond': {
                            'if':  '$events.shot_result',
                            'then': 0,
                            'else': 1}}}, }
                    },

                    {'$group': {
                        '_id': {'team': '$_id.team'},
                        'types': {
                            '$push': {
                                'type': '$_id.shot_type',
                                **{i: f'${i}' for i in ['attemps', 'made', 'miss']}}}
                            }
                     }

                    ],
                'as': 'scoring'
                }}]

    @property
    def post_cleanup(self):
        return [{'$project': {'scoring': 1}}]

    def draft(self):
        pass


def events_report(time_marks=None):
    cur = db.games.aggregate([
        {'$match': {'events': True}},
        {'$lookup': {
            'from': 'events',
            'let': {'game': '$_id'},
            'pipeline': [
                {'$match': {'$expr': {'$eq': ['$game', '$$game']}}},
                {'$unwind': '$events'},
                {'$match': {'$expr': {'$eq': ['投籃', '$events.type']}}},

                # if time marks exists, only get results from the time span.
                # * if time_marks is not None else None for i in [1]],

                # only show team difference.
                {'$group': {'_id': {
                    'team': '$team',
                    'shot_type': '$events.shot_type',
                    },
                    'attemps': {'$sum': 1},
                    'made': {'$sum': {'$cond': {'if':  '$events.shot_result', 'then': 1, 'else': 0}}},
                    'miss': {'$sum': {'$cond': {'if':  '$events.shot_result', 'then': 0, 'else': 1}}},
                    # 'events': {'$push': '$events'}
                    }
                 },

                {'$group': {
                    '_id': {'team': '$_id.team'},
                    'types': {
                        '$push': {
                            'type': '$_id.shot_type',
                            **{i: f'${i}' for i in ['attemps', 'made', 'miss']}}}
                            }
                 }

                # show player splits.
                # {'$group': {'_id': {
                #      'team': '$team',
                #      'shot_type': '$events.shot_type',
                #      'player': '$events.player'},
                #      'counts': {'$sum': 1},
                #      'made': {'$sum': {'$cond': {'if':  '$events.shot_result', 'then': 1, 'else': 0}}},
                #      'miss': {'$sum': {'$cond': {'if':  '$events.shot_result', 'then': 0, 'else': 1}}},
                #      'events': {'$push': '$events'}
                #      }
                #  },
            ],
            'as': 'scoring'
         }},
        {'$project': {'scoring': 1}}
    ])

    doc = list(cur)
    pprint(doc)


# roster()
graph = ScoringGraph(col=db.games, team='成功大學')
pprint(graph.pipelines)
doc = graph.invoke()
pprint(doc)
