from pprint import pprint

from pymongo import MongoClient

client = MongoClient()
db = client["games_report"]
col = db["events"]

pipeline = [
    {"$match": {"events.shot_result": {"$exists": True}}},
    {"$unwind": "$events"},
    {
        "$facet": {
            "twos": [
                {"$match": {"events.shot_type": "2分"}},
                {
                    "$group": {
                        "_id": "$player",
                        "avg": {"$avg": {"$toInt": "$events.shot_result"}},
                        "score": {
                            "$sum": {
                                "$multiply": [{"$toInt": "$events.shot_result"}, 2]
                            }
                        },
                        "counts": {"$sum": 1},
                    }
                },
                {"$match": {"counts": {"$gte": 5}}},
                {
                    "$bucketAuto": {
                        "groupBy": "$avg",
                        "buckets": 5,
                        "output": {
                            "nominee": {"$push": {"player": "$_id", "rank": "$avg"}}
                        },
                    }
                },
                {"$skip": 4},
                {"$unwind": "$nominee"},
            ],
            "threes": [
                {"$match": {"events.shot_type": "3分"}},
                {
                    "$group": {
                        "_id": "$player",
                        "avg": {"$avg": {"$toInt": "$events.shot_result"}},
                        "score": {
                            "$sum": {
                                "$multiply": [{"$toInt": "$events.shot_result"}, 3]
                            }
                        },
                        "counts": {"$sum": 1},
                    }
                },
                {"$match": {"counts": {"$gte": 5}}},
                {
                    "$bucketAuto": {
                        "groupBy": "$avg",
                        "buckets": 5,
                        "output": {
                            "nominee": {"$push": {"player": "$_id", "rank": "$avg"}}
                        },
                    }
                },
                {"$skip": 4},
                {"$unwind": "$nominee"},
            ],
        }
    },
    {
        "$addFields": {
            "top_both": {
                "$setIntersection": ["$threes.nominee.player", "$twos.nominee.player"]
            }
        }
    }
    # {'$sortBy': {}}
    # {'$facet': {'threes': {'$match'}}}
    # {
    #     "$facet": {
    #         "threes": [
    #             {"$match": {"events.shot_type": "3分"}},
    #             {"$bucketAuto": {'groupBy': "$player", 'buckets': 10, }},
    #         ]
    #     }
    # },
]


for doc in col.aggregate(pipeline):
    pprint(doc)
