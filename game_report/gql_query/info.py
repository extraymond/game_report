import graphene
import pendulum
from loguru import logger

from game_report.database import db


class Shooting(graphene.ObjectType):

    fga = graphene.Int()
    fgm = graphene.Int()
    fgper = graphene.Float()


class Rotation(graphene.ObjectType):
    def resolve_mp(self, info):
        seconds = self.session[1] - self.session[0]
        return pendulum.from_format(f"{seconds}", "X").format("mm:ss")

    async def resolve_boxplus(self, info):
        start, end = self.session
        pipeline = [
            {
                "$match": {
                    "game": self._id,
                    "seconds": {"$gte": start, "$lte": end},
                    "events.shot_result": True,
                }
            },
            {"$unwind": "$events"},
            {
                "$addFields": {
                    "points": {
                        "$switch": {
                            "branches": [
                                {
                                    "case": {"$eq": ["$events.shot_type", "3分"]},
                                    "then": 3,
                                },
                                {
                                    "case": {"$eq": ["$events.shot_type", "2分"]},
                                    "then": 2,
                                },
                            ],
                            "default": 1,
                        }
                    }
                }
            },
            {
                "$group": {
                    "_id": None,
                    "boxplus": {
                        "$sum": {
                            "$cond": {
                                "if": {"$eq": ["$team", self.team]},
                                "then": "$points",
                                "else": {"$multiply": [-1, "$points"]},
                            }
                        }
                    },
                }
            },
        ]

        async for i in db.events.aggregate(pipeline):
            return i["boxplus"]
        else:
            return 0.0

    async def resolve_shooting(self, info):
        start, end = self.session
        pipeline = [
            {
                "$match": {
                    "game": self._id,
                    "seconds": {"$gte": start, "$lte": end},
                    "events.shot_type": {"$exists": True},
                    "team": self.team,
                }
            },
            {"$unwind": "$events"},
            {
                "$group": {
                    "_id": None,
                    "fga": {"$sum": 1},
                    "fgper": {"$avg": {"$toInt": "$events.shot_result"}},
                    "fgm": {
                        "$sum": {
                            "$cond": {
                                "if": {"$eq": ["$events.shot_result", True]},
                                "then": 1,
                                "else": 0,
                            }
                        }
                    },
                }
            },
            {
                "$addFields": {
                    "fgper": {
                        "$divide": [{"$floor": {"$multiply": ["$fgper", 10000]}}, 100]
                    }
                }
            },
            {"$project": {"_id": 0}},
        ]

        async for i in db.events.aggregate(pipeline):
            return Shooting(**i)
        else:
            return {"fga": 0, "fgm": 0, "fgper": None}

    shooting = graphene.Field(Shooting)
    lineup = graphene.List(graphene.String)
    session = graphene.List(graphene.Float)
    _id = graphene.String(name="game")
    team = graphene.String()
    boxplus = graphene.Float()
    mp = graphene.String(resolver=resolve_mp)


class Team(graphene.ObjectType):

    team = graphene.String()
    _id = graphene.String(name="id")
    rosters = graphene.List(graphene.String)
    rotations = graphene.List(Rotation, players=graphene.List(graphene.String))

    async def resolve_rosters(self, info):
        return [
            i["player"]
            async for i in db.players.aggregate([{"$match": {"team": self._id}}])
        ]

    async def resolve_rotations(self, info, players=None):
        pipeline = [
            {"$match": {"events": True}},
            {"$unwind": "$box"},
            {"$match": {"box.先發": True, "box.team": self.team}},
            {"$group": {"_id": "$_id", "starters": {"$push": "$box.player"}}},
            {
                "$lookup": {
                    "from": "events",
                    "let": {"game": "$_id"},
                    "pipeline": [
                        {"$match": {"$expr": {"$eq": ["$game", "$$game"]}}},
                        {
                            "$facet": {
                                "endtime": [
                                    {"$match": {"events.type": "endgame"}},
                                    {"$limit": 1},
                                    {"$project": {"seconds": 1, "_id": 0}},
                                ],
                                "timings": [
                                    {
                                        "$match": {
                                            "events.type": "換人",
                                            "team": self.team,
                                        }
                                    },
                                    {
                                        "$group": {
                                            "_id": None,
                                            "seconds": {"$addToSet": "$seconds"},
                                        }
                                    },
                                    {"$unwind": "$seconds"},
                                    {"$sort": {"seconds": 1}},
                                    {
                                        "$group": {
                                            "_id": None,
                                            "seconds": {"$push": "$seconds"},
                                        }
                                    },
                                ],
                            }
                        },
                        {"$unwind": "$timings"},
                        {
                            "$project": {
                                "sessions": {
                                    "$zip": {
                                        "inputs": [
                                            {
                                                "$concatArrays": [
                                                    [0.0],
                                                    "$timings.seconds",
                                                ]
                                            },
                                            {
                                                "$concatArrays": [
                                                    "$timings.seconds",
                                                    "$endtime.seconds",
                                                ]
                                            },
                                        ]
                                    }
                                }
                            }
                        },
                        {"$unwind": "$sessions"},
                    ],
                    "as": "rotations",
                }
            },
            {"$unwind": "$rotations"},
            {
                "$lookup": {
                    "from": "events",
                    "let": {
                        "time": {"$arrayElemAt": ["$rotations.sessions", 0]},
                        "game": "$_id",
                        "starters": "$starters",
                    },
                    "pipeline": [
                        {"$match": {"events.type": "換人", "team": self.team}},
                        {
                            "$match": {
                                "$expr": {
                                    "$and": [
                                        {"$eq": ["$game", "$$game"]},
                                        {"$lte": ["$seconds", "$$time"]},
                                    ]
                                }
                            }
                        },
                        {"$unwind": "$events"},
                        {
                            "$group": {
                                "_id": "$events.player",
                                "sub_record": {
                                    "$sum": {
                                        "$cond": {
                                            "if": {"$eq": ["$events.sub_type", "in"]},
                                            "then": 1,
                                            "else": -1,
                                        }
                                    }
                                },
                            }
                        },
                        {
                            "$group": {
                                "_id": None,
                                "lineups": {
                                    "$push": {
                                        "player": "$_id",
                                        "sub_record": "$sub_record",
                                    }
                                },
                            }
                        },
                        {
                            "$addFields": {
                                "lineups": {
                                    "$concatArrays": [
                                        "$lineups",
                                        {
                                            "$map": {
                                                "input": "$$starters",
                                                "in": {
                                                    "player": "$$this",
                                                    "sub_record": 1,
                                                },
                                            }
                                        },
                                    ]
                                }
                            }
                        },
                        {"$unwind": "$lineups"},
                        {"$sort": {"lineups.player": 1}},
                        {
                            "$group": {
                                "_id": "$lineups.player",
                                "sub_record": {"$sum": "$lineups.sub_record"},
                            }
                        },
                        {"$match": {"sub_record": {"$gt": 0}}},
                    ],
                    "as": "lineup",
                }
            },
            {
                "$addFields": {
                    "lineup": {
                        "$cond": {
                            "if": {"$eq": ["$lineup", []]},
                            "then": "$starters",
                            "else": "$lineup._id",
                        }
                    }
                }
            },
            {
                "$project": {
                    "lineup": "$lineup",
                    "session": "$rotations.sessions",
                    "team": self.team,
                }
            },
        ]
        return [Rotation(**i) async for i in db.games.aggregate(pipeline)]


class Game(graphene.ObjectType):

    teams = graphene.List(graphene.String)
    _id = graphene.String(name="id")
    result = graphene.List(graphene.Int)
    winner = graphene.String()

    async def resolve_result(self, info):

        async for i in db.games.aggregate(
            [{"$match": {"_id": self._id}}, {"$project": {"result": "$result.total"}}]
        ):
            return i["result"]

    async def resolve_winner(self, info):
        async for i in db.games.aggregate(
            [
                {"$match": {"_id": self._id}},
                {"$unwind": "$result"},
                {"$sort": {"result.total": -1}},
                {"$limit": 1},
                {"$addFields": {"winner": "$result.team"}},
            ]
        ):
            return i["winner"]
