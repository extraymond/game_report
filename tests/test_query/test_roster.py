import pytest
from loguru import logger

from game_report.query import roster


@pytest.mark.asyncio
async def test_roster():
    cursor = await roster.get_roster()

    logger.debug(cursor)

    docs = []
    async for doc in cursor:
        logger.info(doc)
        docs.append(doc)

    assert docs != None
