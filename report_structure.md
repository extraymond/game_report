This represents the things I would like to gather from the games.

# 1. Gather information from play by plays

## Lineups analysis.

### Performance across lineups.

### High performance lineups.

### Utilization of team.

## Analyze minutes played

### Peaks of both ends

### Performance across game.

## Game analysis

# 2. Double check with box_scores

# 3. Video analysis of games
