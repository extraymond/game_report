# Scouts report

## Team overview

1. Lineups
2. Depth order
3. Utilization of team

## Offense Review

1. Most often called
2. Lesser called
3. Special
4. Performance against opponents

## Defense Review

1. Most often called
2. Lesser called
3. Special
4. Performance against opponents

## Personnel Review

1. Strength
2. Weakness

## Tactical Suggestion

1. Offense sets.
2. Defense sets.
3. Match-ups.
